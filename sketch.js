let canvas;
let m;

const cellsCompteurs = $('#counters-container table tr td.compteur');
const cssRoot = $(':root');
//Dimension de la fenetre
let LARGEUR = 800;
let HAUTEUR = 600;
let FR = 60;



const COLORS = [
	cssRoot.css('--sain'),
	cssRoot.css('--infecte'),
	cssRoot.css('--symptome'),
	cssRoot.css('--grave'),
	cssRoot.css('--mort'),
	cssRoot.css('--immune')
];

//Taille d'un individu dans la fenetre
let RAYON = 3;

//Amplitude max du mouvement d'un individu
let AMPLITUDE = 100;

//Taille de la population
let TAILLE_POPULATION = 200;

let CAPACITE_HOPITAL = 20;

//OPTION
let OPTIONS = {};


function Monde() {
	this.largeur = LARGEUR;
	this.hauteur = HAUTEUR;

	this.hopital = new Hopital();

	this.population = {
		taille: TAILLE_POPULATION,
		gens: []
	};
	for (let i = 0; i < TAILLE_POPULATION; i++) {
		this.population.gens.push(new Personne(this))
	}

	this.compteur = [this.population.taille];
	for(let i=1; i<6; i++){
		this.compteur.push(0);
	}
	cellsCompteurs.eq(0).text(this.population.taille);
	cellsCompteurs.filter(':not(:first-child)').text(0);


	this.hoveredPoints = [];

	this.update = function () {

		this.hopital.draw();

		if(canvas.isLooping) {
			this.population.gens.forEach(function (p, i) {
				p.move();
			});
			this.population.gens.forEach(function (p, i) {
				p.update();
			});
		} else {
			this.population.gens.forEach(function (p) {
				p.draw();
			});
		}

		if(this.hoveredPoints.length>0) {
			let closestPoint = this.hoveredPoints[0];
			let mousePos = {
				x: mouseX,
				y: mouseY
			};
			this.hoveredPoints.forEach(function(p) {
				if(distance(p.pos, mousePos) < distance(closestPoint.pos, mousePos)) {
					closestPoint = p;
				}
			});

			noStroke();
			fill('rgba(255,255,255,0.5)');
			circle(closestPoint.pos.x, closestPoint.pos.y, RAYON * 2 + 4);
			closestPoint.draw();

			$('#sketch-holder').addClass('pointer');
			this.currentlyHovered = closestPoint;
		}
		else {
			$('#sketch-holder').removeClass('pointer');
			delete this.currentlyHovered;
		}


		this.updateCompteur();
	};

	this.patient0 = function () {
		this.population.gens[randomBorneInt(0, this.population.taille-1)].tombeMalade();
		this.updateCompteur();
	};

	this.changementCompteur = function(ancienEtat, nouvelEtat){
		this.compteur[ancienEtat]-=1;
		this.compteur[nouvelEtat]+=1;
	};

	this.updateCompteur = function(){
		this.compteur.forEach(function (nb,etat) {
			cellsCompteurs.eq(etat).text(nb);
		});
	};

	this.areCoordOutside = function(x,y){
		return x < RAYON ||
			x > this.largeur-RAYON ||
			y < RAYON ||
			y > this.hauteur-RAYON;
	};
}

/**
 * @param monde : Monde
 * @constructor
 */
function Personne(monde) {
	this.monde = monde;

	this.pos = {
		x:0,
		y:0
	};
	do{
		this.pos = {
			x: LARGEUR * random(),
			y: HAUTEUR * random()
		};
	} while(this.monde.hopital.areCoordInside(this.pos.x, this.pos.y));

	this.home = {
		x: this.pos.x,
		y: this.pos.y
	};

	this.target = {
		x: this.pos.x,
		y: this.pos.y,
		distance: 0,
		xDir: 0,
		yDir: 0,
	};

	/**
	 * 0 = sain
	 * 1 = malade asympotmatique
	 * 2 = malade avec symptomes
	 * 3 = gravement malade
	 * 4 = mort
	 * 5 = imunisé
	 * @type {number}
	 */
	this.etat = 0;
	this.resistance=randomBorne(1,3);
	this.envieHopital=randomBorne(0,1);

	this.isHovered = false;

	this.tombeMalade = function () {
		this.monde.changementCompteur(this.etat,1);
		this.etat = 1;

		this.delaiProchainEtat = Math.round(OPTIONS.delaiSymptome * FR * this.resistance);
	};

	this.developpeSymptome = function () {
		this.monde.changementCompteur(this.etat,2);
		this.etat = 2;

		this.delaiProchainEtat = Math.round(OPTIONS.delaiAggravation * FR * this.resistance);
		this.delaiHopital = Math.round(this.delaiProchainEtat * this.envieHopital);

		this.toHome();
	};

	this.aggrave = function () {
		this.monde.changementCompteur(this.etat,3);
		this.etat = 3;

		this.delaiProchainEtat = Math.round(OPTIONS.delaiMort * FR * this.resistance);
	};

	this.meurt = function () {
		this.monde.changementCompteur(this.etat,4);
		this.etat = 4;
		delete this.delaiProchainEtat;
		delete this.delaiHopital;

		if(this.estHospitalise()){
			this.monde.hopital.removeSomeone(this);
			this.pos = {
				x: this.teleportedFrom.x,
				y: this.teleportedFrom.y
			}
		}

		this.teleportTo(
			300 + randomBorne(-2*RAYON, 2*RAYON),
			300 + randomBorne(-2*RAYON, 2*RAYON)
		);
	};

	this.guerit = function () {
		this.monde.changementCompteur(this.etat,5);
		this.etat=5;
		delete this.delaiProchainEtat;
		delete this.delaiHopital;
		if(this.estHospitalise()){
			console.log(this.hospitalise);
			this.monde.hopital.removeSomeone(this);
		}
	};

	this.toHome = function () {
		this.target.x = this.home.x;
		this.target.y = this.home.y;
		const newTarget = coord2trigo(this.home.x - this.pos.x, this.home.y - this.pos.y);
		this.target.distance = Math.round(newTarget.distance);
		this.target.xDir = newTarget.cos;
		this.target.yDir = newTarget.sin;

	};

	this.toHopital = function() {
		if(!this.monde.hopital.estPlein()) {
			/*this.teleportedFrom = {
				x: this.pos.x,
				y: this.pos.y
			};*/

			this.monde.hopital.addSomeone(this);
		}
		else {
			this.hospitalise = {
				etat: 1,
				nLit:undefined
			}
		}

	};

	this.estHospitalise = function() {
		return this.hasOwnProperty('hospitalise') && this.hospitalise.etat===2;
	};

	this.estEnAttenteHopital = function() {
		return this.hasOwnProperty('hospitalise') && this.hospitalise.etat===1;
	};

	this.update = function () {
		//devient malade
		if (this.etat === 0) {
			this.monde.population.gens.some(function (autre) {
				if (autre.etat !== 5
					&& autre.etat !== 0
					&& autre.etat !== 4
					&& distanceC(this.pos, autre.pos) <= (OPTIONS.distanceContamination + 2 * RAYON) * (OPTIONS.distanceContamination + 2 * RAYON)
					&& random() < OPTIONS.propagation) {
					this.tombeMalade();
					return true;
				}
				return false;
			}, this)
		}

		//évolution maladie
		else if (this.etat === 1 || this.etat===2 || this.etat===3) {
			//guérison
			let proba = OPTIONS.probaGuerison;
			if(this.estHospitalise()) //3x plus de chance de guérir si hopital
				proba *=3;

			if(random() < proba ){
				this.guerit();
			}
			else {
				if(this.etat === 2 && this.delaiHopital>0){
					this.delaiHopital--;
				}

				this.delaiProchainEtat--;
				if (this.delaiProchainEtat === 0) {
					switch (this.etat) {
						case 1:
							this.developpeSymptome();
							break;
						case 2:
							this.aggrave();
							break;
						case 3:
							this.meurt();
							break
					}

				}
			}
		}
	};

	this.move = function () {
		if (! (this.etat === 4 ||
			   this.estHospitalise() ||
			   ((this.etat===2 || this.etat===3) && this.target.distance===0) ) ) {

			if (this.target.distance === 0 || this.monde.hopital.areCoordInside(this.pos.x + this.target.xDir, this.pos.y + this.target.yDir)) {
				this.newRandomTarget();
			}

			this.doStep();
			if( (this.etat===2) && this.target.distance!==0){
				this.doStep()
			}
		}
		else if(!this.estHospitalise() && ((this.etat === 2 && this.delaiHopital===0) || this.etat === 3)){
			this.toHopital();
		}

		this.draw();
	};

	this.teleportTo = function(x,y) {
		this.teleportedFrom = {
			x:this.pos.x,
			y:this.pos.y,
			lineFade:255
		};
		this.pos = {
			x:x,
			y:y
		};
	};

	this.doStep = function () {
		this.pos.x += this.target.xDir;
		this.pos.y += this.target.yDir;
		this.target.distance--;
	};

	this.newRandomTarget = function(){
		do{
			const angle = -randomBorne(0, 2*Math.PI);
			this.target.xDir = Math.cos(angle);
			this.target.yDir = Math.sin(angle); // -angle car y=0 est en haut
			this.target.distance = randomBorneInt(1, AMPLITUDE);

			const coord = trigo2coord(this.target.distance, this.target.xDir, this.target.yDir);
			this.target.x = coord.x + this.pos.x;
			this.target.y = coord.y + this.pos.y;

		} while(this.monde.areCoordOutside(this.target.x, this.target.y) ||
			this.monde.hopital.areCoordInside(this.target.x, this.target.y));
	};

	this.draw = function () {
		const distMouse = distance(this.pos, {x:mouseX,y:mouseY});
		if(!this.isHovered && canvas.isHovered && distMouse <= RAYON + 5)
		{
			this.monde.hoveredPoints.push(this);
			this.isHovered = true;
		}
		else if(this.isHovered && (!canvas.isHovered || distMouse > RAYON + 5)) {
			this.monde.hoveredPoints.splice(this.monde.hoveredPoints.indexOf(this),1);
			this.isHovered = false;
		}

		noStroke();
		fill(COLORS[this.etat]);
		circle(this.pos.x, this.pos.y, RAYON * 2);

		if(this.hasOwnProperty('teleportedFrom')){
			let lineColor = color(COLORS[this.etat]);
			lineColor.setAlpha(this.teleportedFrom.lineFade);
			stroke(lineColor);
			strokeWeight(2);
			line(this.teleportedFrom.x, this.teleportedFrom.y, this.pos.x, this.pos.y);
			if(this.teleportedFrom.lineFade===0){
				delete this.teleportedFrom
			}
			else if(canvas.isLooping) {
				this.teleportedFrom.lineFade-=15;
			}
		}
	};
}

function Hopital() {
	this.pos = {
		x: 0,
		y: 0
	};
	this.wallWidth = 2;
	this.width = 100;
	this.innerWidth = this.width - this.wallWidth*2;
	this.height= 100;
	this.innerHeight = this.height - this.wallWidth*2;

	this.entrance = {
		x:this.pos.x + this.width +1,
		y:this.pos.y + this.height +1
	};

	this.capacite = CAPACITE_HOPITAL;
	this.nbLitsOccupes = 0;
	this.lits = [];
	for (let i = 0; i < CAPACITE_HOPITAL; i++) {
		this.lits.push(false)
	}

	this.estPlein = function () {
		return this.nbLitsOccupes === this.capacite
	};

	/**
	 *
	 * @param p : Personne
	 */
	this.addSomeone = function(p) {
		if(!this.estPlein()){
			this.lits.some(function(lit,nLit) {
				const nbPlaceX = Math.floor((this.innerWidth-20)/(2*RAYON))+1;
				if(lit===false){
					this.lits[nLit]=true;
					p.hospitalise = {
						etat:2,
						nLit:nLit
					};

					p.teleportTo(
						this.pos.x + this.wallWidth + 10 + 2*RAYON * (nLit%nbPlaceX),
						this.pos.y + this.wallWidth + 10 + 2*RAYON * (Math.floor(nLit/nbPlaceX))
					);
					this.nbLitsOccupes++;

					p.target = {
						xDir : 0,
						yDir : 0,
						distance : 0,
						x: p.pos.x,
						y: p.pos.y
					};

					return true;
				}
				return false;
			},this);
		}
	};

	/**
	 * @param p : Personne
	 */
	this.removeSomeone = function(p) {
		this.lits[p.hospitalise.nLit] = false;
		this.nbLitsOccupes--;
		delete p.hospitalise;

		p.teleportTo(this.entrance.x, this.entrance.y);
	};

	this.draw = function () {
		stroke('red');
		strokeWeight(this.wallWidth);
		fill(96);
		rect(this.pos.x+this.wallWidth/2, this.pos.y+this.wallWidth/2, this.width-this.wallWidth, this.height-this.wallWidth);

		noStroke();
		fill(255);
		textSize(12);
		textAlign(RIGHT);
		text(this.nbLitsOccupes + '/' + this.capacite, this.pos.x + this.width - this.wallWidth, this.pos.y + this.height - this.wallWidth);
	};

	this.areCoordInside = function(x,y){
		return x >= this.pos.x-RAYON &&
			x <= this.pos.x + this.width + RAYON &&
			y >= this.pos.y-RAYON &&
			y <= this.pos.y + this.height + RAYON;
	};
}


function setup() {
	canvas = createCanvas(LARGEUR, HAUTEUR);
	canvas.parent('sketch-holder');
	frameRate(FR);

	canvas.isLooping=false;
	canvas.mouseOver(mouseOver);
	canvas.mouseOut(mouseOut);

	OPTIONS = {
		propagation: 5 / 100,
		distanceContamination: 5,

		delaiSymptome: 5,
		delaiAggravation: 3,
		delaiMort: 3,

		probaGuerison: 1/1000
	};

	m = new Monde();
	m.patient0();

	noLoop();
}

function draw() {
	clear();
	background(64);
	m.update();
}

function mouseMoved() {
	if(mouseX>=0 && mouseY>=0 && mouseX<=width && mouseY<=height){
		if(!canvas.isLooping){
			redraw()
		}
	}
}

function mouseOver() {
	canvas.isHovered = true;
}

function mouseOut() {
	canvas.isHovered = false;
}