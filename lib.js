function randomBorne(min,max){
	return Math.random() * (max - min) + min;
}
function randomBorneInt(min,max){
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function approche(a, b, ecart = 1 , inclus= false) {
	if(inclus)
		return Math.abs(a-b) <= ecart;
	else
		return Math.abs(a-b) < ecart;
}

function distanceC(p1, p2) {
	return (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y)
}

function distance(pos1, pos2) {
	return Math.sqrt((pos1.x-pos2.x)*(pos1.x-pos2.x) + (pos1.y-pos2.y)*(pos1.y-pos2.y))
}

function coord2trigo(x,y) {
	const dist = distance({x:0,y:0},{x:x,y:y});
	const sincos = {
		cos : x/dist,
		sin : y/dist,
	};
	return {
		distance : dist,
		sin: sincos.sin,
		cos: sincos.cos,
	}
}
function trigo2coord(distance, cos, sin) {
	return{
		x: cos*distance,
		y: sin*distance,
	}
}