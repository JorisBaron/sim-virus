$(document).ready(function () {
	const goButton = $('#go');
	const pauseButton = $('#pause');
	const resetButton = $('#reset');

	const canvasElement = $('#defaultCanvas0');

	goButton.click(function () {
		loop();
		canvas.isLooping = true;
	});

	pauseButton.click(function () {
		noLoop();
		canvas.isLooping = false;
	});

	resetButton.click(function () {
		m = new Monde();
		m.patient0();

		redraw();
	});

	canvasElement.click(function (e) {
		console.log(m.currentlyHovered);
	});

	/*
	canvasElement.mousemove(function (e) {
		const offset = canvasElement.offset();
		canvas.mousePos = {
			x: e.pageX - offset.left,
			y: e.pageY - offset.top
		};
		if(!canvas.isLooping){
			redraw();
		}
	});

	canvasElement.mouseleave(function (e) {

		delete canvas.mousePos;
		if(!canvas.isLooping){
			redraw();
		}
	})
	*/

});